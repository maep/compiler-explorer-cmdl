#!/usr/bin/env python3
# license: WTFPL
# repo: https://gitlab.com/maep/compiler-explorer-cmdl

import os, re, sys
from os.path import isfile, splitext
from urllib.request import urlopen
from urllib.error import HTTPError

HELP = '''
Syntax: compiler-explorer [options] [compiler-options] [source-file]
Command line access to godbolt.org. The source file must be the last argument.
The language may be detected from the file extension.
Options:
    -compiler:ID        Set compiler to compiler-id
    -list-languages     List supported languages
    -list-compilers:ID  List compilers for language-id
'''
ROOT = 'https://godbolt.org/api/'
# language-id: (default-compiler regex, [file extensions])
LANGUAGES = {
    'ada'     : (r'gnat\d+',      ['.adb', '.ads']),
    'assembly': (r'nasm\d+',      ['.S']),
    'c'       : (r'cg\d+',        ['.c']),
    'c++'     : (r'g\d+',         ['.C', '.cc', '.cpp', '.cxx', '.c++']),
    'clean'   : (r'clean\d+_64',  ['.icl', '.dcl', '.abc']),
    'cuda'    : (r'nvcc\d+',      ['.cu']),
    'd'       : (r'ldc1_\d+',     ['.d']),
    'fortran' : (r'gfortran\d+',  ['.f', '.for', '.f90']),
    'go'      : (r'gl\d{4,}',     ['.go']),
    'haskell' : (r'ghc\d+',       ['.hs', '.lhs']),
    'ocaml'   : (r'ocaml.+',      ['.ml', '.mli']),
    'rust'    : (r'r\d{4,}',      ['.rs']),
    'swift'   : (r'swift\d+',     ['.swift']),
    'zig'     : (r'ztrunk',       ['.zig']),
}
EXTENSIONS = {ext: lang for lang in LANGUAGES for ext in LANGUAGES[lang][1]}

def api_get(endpoint):
    return urlopen(ROOT + endpoint).read().decode('utf8')

def api_post(endpoint, data):
    return urlopen(ROOT + endpoint, data=data.encode('utf8')).read().decode('utf8')

def get_compiler(ext):
    language_id = EXTENSIONS[ext]
    compiler_re = re.compile(LANGUAGES[language_id][0])
    compiers    = api_get('compilers/' + language_id).splitlines()
    return list(filter(compiler_re.match, compiers))[-1].split()[0]

def compile(compiler_id, args, source):
    if len(source) > 200000:
        sys.exit('Source file too large')
    endpoint = 'compiler/%s/compile' % compiler_id
    if args:
        endpoint += '?options=' + '%20'.join(args)
    return api_post(endpoint, source)

def main(args):
    if not args or args[0] == '-h':
        sys.exit(HELP)

    option = args[0]  # first argument may be option
    path   = args[-1] # last argument must be file

    if option == '-list-languages':
        print(api_get('languages'))
    elif option.startswith('-list-compilers:'):
        print(api_get('compilers/' + option[16:]))
    elif not isfile(path):
        sys.exit('Last argument must be a file')
    else:
        ext    = splitext(path)[1]
        source = open(path).read()
        if option[:10] == '-compiler:':
            print(compile(option[11:], args[1:-1], source))
        elif ext in EXTENSIONS:
            print(compile(get_compiler(ext), args[:-1], source))
        else:
            sys.exit('Please provide a compiler-id')

if __name__ == '__main__':
    try:
        main(sys.argv[1:])
    except HTTPError as e:
        sys.exit(str(e))
    except KeyboardInterrupt:
        pass
