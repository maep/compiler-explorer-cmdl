A small python script to access Godbolt's compiler explorer from the command line.

Install:
    wget https://gitlab.com/maep/compiler-explorer-cmdl/raw/master/compiler-explorer
    chmod +x compiler-explorer

Example:
    compiler-explorer hello_world.c

Help:
    compiler-explorer -h

Bugs:
    Compiler flags are ignored. It's prpbably an api bug, even the example doesn't work.
    Default compilers are still hard coded.

License:
    WTFPL (Do What the Fuck You Want To Public License)
